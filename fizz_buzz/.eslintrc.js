module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "jest": true
    },
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "plugins": [
        "react"
    ],
	"extends": "eslint:recommended",
    "rules": {
	    // enable additional rules
        "indent": ["error", "tab"],
        //"linebreak-style": ["error", "unix"],
        "quotes": ["error", "single"],
        "semi": ["error", "always"],
        // override default options for rules from base configurations
        //"comma-dangle": ["error", "always"],
        "no-cond-assign": ["error", "always"],
        "one-var": "off", // ["error", "never"]
        "init-declarations": "off",
        "no-inline-comments": "off",
        // disable rules from base configurations
        "no-console": "off",
        "no-unused-vars": "off",
        "no-irregular-whitespace": ["error"]
    }
};