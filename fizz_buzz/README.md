
                                                FizzBuzz Application
----------------------------------------------------------------------------------------------------------------------


Clone the application by using the below command
git clone https://ChandrashekarMuthumula@bitbucket.org/ChandrashekarMuthumula/aviva-fizz-buzz-chandrashekar_muthumula.git


Install the NPM packages
------------------------
move to "fizz_buzz" folder under the aviva-fizz-buzz-chandrashekar_muthumula and run the command
$ npm install

Run the application
---------------------
$ npm start

Which runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.

Run the unit tests which are updated
------------------------------------
$ npm test

Which runs all the tests that are updated after the previous commit

Run all the unit tests
--------------------
$ npm run test:all

Which runs all the tests created in the application

Debug the unit tests
---------------------
Run the command $npm run test:debug

Which creates the Debugger and it will listen on websocket 
open the chrome browser and enter the url "chrome://inspect" in url bar
Which load all the devices and Remote target
Click on inspect link in the remote target

Deployment
-----------
npm run build
Builds the app for production to the build folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

The build folder is ready to be deployed.
You may serve it with a static server:

To create a static server run 
$ npm install -g serve

To deploy the build in static server
$ serve -s build
