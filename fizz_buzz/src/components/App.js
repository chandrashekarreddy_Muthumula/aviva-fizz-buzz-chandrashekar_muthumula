import React from 'react';
import '../css/App.css';
import PropTypes from 'prop-types';
import FizzBuzz from './FizzBuzz';

const App = ({setFizzBuzzList, fizzBuzzList, currPage}) => (
	<div className='app'>
		<header className='app-header'>
			<span>FizzBuzz</span>
		</header>
		<FizzBuzz updateFizzBuzzList={(fizzBuzzList) => {setFizzBuzzList(fizzBuzzList);}}
			list={fizzBuzzList} currPage={currPage}/>
		<footer className='app-footer'></footer>
	</div>
);

App.propTypes = {
	setFizzBuzzList: PropTypes.func.isRequired,
	fizzBuzzList: PropTypes.array.isRequired,
	currPage: PropTypes.number.isRequired
};
export default App;
