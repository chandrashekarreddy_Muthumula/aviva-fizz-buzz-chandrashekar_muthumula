import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import FizzBuzz from './FizzBuzz';

describe('App', () => {
	let wrapper = null;
	let props = {
		setFizzBuzzList: jest.fn(),
		fizzBuzzList: [],
		currPage: 1
	};
	it('should render a <div />', () => {
		const wrapperApp = shallow(<App {...props}/>);
		expect(wrapperApp.find('div').length).toEqual(1);
	});
 
	it('should render the FizzBuzz Component', () => {
		debugger;
		const wrapper = shallow(<App  {...props}/>);
		expect(wrapper.find(FizzBuzz).length).toEqual(1);
	});

});
