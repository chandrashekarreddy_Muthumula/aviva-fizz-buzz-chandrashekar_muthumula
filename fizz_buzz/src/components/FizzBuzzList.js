import React, { Component } from 'react';
import * as fizzBuzzConst from '../constants/FizzBuzzConst';

class FizzBuzzList extends Component{
    
    _getFizzBuzzListItems = () => {
        if(this.props.items && this.props.items.length)
        {
            let day = new Date().getDay();
            let currPageItems = this.props.items.slice(((this.props.currPage-1)*fizzBuzzConst.MAX_SIZE_OF_PAGE), ((this.props.currPage-1)*fizzBuzzConst.MAX_SIZE_OF_PAGE)+fizzBuzzConst.MAX_SIZE_OF_PAGE);
            return currPageItems.map((item,index) => {
                if(item.val === fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZBUZZ){
                    return (<li className={item.key} key={item.key+index}>
                                <span className={fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZ}>{day === fizzBuzzConst.WEDNESDAY_NUM ? fizzBuzzConst.WEDNESDAY_CODE.FIZZ : fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZ}</span>
                                <span className={fizzBuzzConst.FIZZ_BUZZ_KEYS.BUZZ}>{day === fizzBuzzConst.WEDNESDAY_NUM ? fizzBuzzConst.WEDNESDAY_CODE.BUZZ : fizzBuzzConst.FIZZ_BUZZ_KEYS.BUZZ}</span>
                    </li>) ;
                } 
                return <li className={item.key} key={item.key+index}>{day === fizzBuzzConst.WEDNESDAY_NUM ? (fizzBuzzConst.WEDNESDAY_CODE[item.val] || [item.val]) : [item.val]}</li> 
            }
          );
        }
    }
    render(){
        let fizzBuzzItems = this._getFizzBuzzListItems();
        return (<div>
            <ul className='fizz-buzz-list'>
            {fizzBuzzItems}
            </ul>
        </div>);
    }
}

export default FizzBuzzList;