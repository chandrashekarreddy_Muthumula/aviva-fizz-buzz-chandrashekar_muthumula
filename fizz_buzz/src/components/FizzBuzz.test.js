import React from 'react';
import { shallow } from 'enzyme';
import FizzBuzz from './FizzBuzz';
import Pagination from './Pagination';
import * as FizzBuzzValidations from '../validations/FizzBuzz';
import { createRenderer } from 'react-test-renderer/shallow';
import * as fizzBuzzConst from '../constants/FizzBuzzConst';

describe('FizzBuzz', () => {
	let wrapper;
	const props = {
		updateFizzBuzzList: jest.fn(),
		list: []
	};
	window.alert = () => {};  // provide an empty implementation for window.alert
	beforeEach(() => wrapper = shallow(<FizzBuzz {...props}/>));

	it('should render a <section />', () => {
		expect(wrapper.find('section').length).toEqual(1);
   
	});
	it('should have the Pagination Component', () => {
		expect(wrapper.find('section').props().children[1].type.WrappedComponent.name).toEqual('Pagination');
	});
	it('should render the section element with class main', () => {
		//debugger;
		expect(wrapper.find('section').props().className).toBe('main');
	});

	it('_validateAndDoFizzBuzz should call validateFizzBuzz', () => {
		const instance = wrapper.instance();
		const spy = jest.spyOn(FizzBuzzValidations, 'default');
		instance._validateAndDoFizzBuzz({preventDefault: jest.fn()});
		expect(spy).toHaveBeenCalled();
	});
	it('_validateAndDoFizzBuzz should call updateFizzBuzzList', () => {
		debugger;
		const instance = wrapper.instance();
		FizzBuzzValidations.default.mockReturnValue(true);
		const spy = jest.spyOn(instance.props, 'updateFizzBuzzList');
		instance._validateAndDoFizzBuzz({preventDefault: jest.fn()});
		expect(spy).toHaveBeenCalled();
	});
	it('_doFizzBuzz should generate the fizzbuzz', () => {
		const instance = wrapper.instance();
		const spy = jest.spyOn(instance.props, 'updateFizzBuzzList');
		let fizzBuzzList = instance._doFizzBuzz(20);
		expect(fizzBuzzList[14].val).toEqual(fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZBUZZ);
		expect(fizzBuzzList[2].val).toEqual(fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZ);
		expect(fizzBuzzList[4].val).toEqual(fizzBuzzConst.FIZZ_BUZZ_KEYS.BUZZ);
		expect(fizzBuzzList[5].key).toEqual(fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZ);
	});
});
