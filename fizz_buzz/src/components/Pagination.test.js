import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Pagination from './Pagination';

const setup = ( editing = false ) => {
	const props = {
		currPage: 1,
		setFizzBuzzList: jest.fn(),
		setCurrPageNum: jest.fn(),
		list: []
    
	};

	const renderer = createRenderer();

	renderer.render(
		<Pagination {...props} />
	);

	let output = renderer.getRenderOutput();
 
	return {
		props: props,
		output: output,
		renderer: renderer
	};
};

describe('Pagination', () => {
	it('initial render', () => {
		//debugger;
		const { output } = setup();

		expect(output.type).toBe('div');
		expect(output.props.children[1].type).toBe('button');
	});

	it('go to previous page', () => {
		const { output, props } = setup();
		var prevBtn = output.props.children[1];
		prevBtn.props.onClick();
		expect(props.setCurrPageNum).toHaveBeenCalled();

	});
	it('goto next page', () => {
		const { output, props } = setup();
		var nextBtn = output.props.children[2];
		nextBtn.props.onClick();
		expect(props.setCurrPageNum).toHaveBeenCalled();
	});

	it('prev button should be disabled when current page is 1', () => {
		//debugger;
		const { output } = setup();
		var prevBtn = output.props.children[1];
		expect(prevBtn.props.disabled).toBeTruthy();
	});
});
