import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Pagination from '../containers/Pagination';
import validateFizzBuzz from '../validations/FizzBuzz';
import * as fizzBuzzConst from '../constants/FizzBuzzConst';
class FizzBuzz extends Component {
  static propTypes = {
    updateFizzBuzzList: PropTypes.func.isRequired,
    list: PropTypes.array.isRequired
  };

  state = {
    maxFizzBuzzNo: null
  };

  _handleChange = event => {
    this.setState({ maxFizzBuzzNo: event.target.value });
  };

  _doFizzBuzz = (maxFizzBuzzNo) => {
    var fizzBuzzList = [];
      for (let i = 1; i <= maxFizzBuzzNo; i++) {
        var item =
          i % 15 === 0
            ? { key: fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZBUZZ, val: fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZBUZZ }
            : i % 5 === 0
            ? { key: fizzBuzzConst.FIZZ_BUZZ_KEYS.BUZZ, val: fizzBuzzConst.FIZZ_BUZZ_KEYS.BUZZ }
            : i % 3 === 0
            ? { key: fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZ, val: fizzBuzzConst.FIZZ_BUZZ_KEYS.FIZZ }
            : { key: i, val: i };
        fizzBuzzList.push(item);
      }
      return fizzBuzzList; 
  }

  _validateAndDoFizzBuzz = event => {
    event.preventDefault();
    if(validateFizzBuzz(this.state.maxFizzBuzzNo)){
      let fbList = this._doFizzBuzz(this.state.maxFizzBuzzNo);
      this.props.updateFizzBuzzList(fbList);
    }else{
      alert(fizzBuzzConst.ERRORS.FIZZBUZZ_INVALID_INPUT);
    }
  };

  render() {
    return (
      <section className="main">
        <form className="fizz-buzz-input-form" onSubmit={this._validateAndDoFizzBuzz}>
          <label htmlFor="maxFizzBuzz">
            Enter the max size of FizzBuzz between {fizzBuzzConst.MIN_FIZZ_BUZZ_INPUT}  to {fizzBuzzConst.MAX_FIZZ_BUZZ_INPUT} :
          </label>
          <br />
          <input
            id="maxFizzBuzz"
            type="number"
            min={fizzBuzzConst.MIN_FIZZ_BUZZ_INPUT}
            max={fizzBuzzConst.MAX_FIZZ_BUZZ_INPUT}
            className="max-fizz-buzz-input"
            placeholder={this.props.placeholder}
            autoFocus={true}
            value={this.props.maxFizzBuzzNo}
            onChange = {this._handleChange}
          />
          <button className='primary-btn'>Submit</button>
        </form>
        <Pagination
          list={this.props.list}
          setFizzBuzzList={this.props.updateFizzBuzzList}
        />
      </section>
    );
  }
}

export default FizzBuzz;
