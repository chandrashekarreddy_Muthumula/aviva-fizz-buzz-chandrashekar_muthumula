import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FizzBuzzList from './FizzBuzzList';
import * as fizzBuzzConst from '../constants/FizzBuzzConst';

class Pagination extends Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    currPage: PropTypes.number.isRequired,
    setFizzBuzzList: PropTypes.func.isRequired,
    setCurrPageNum: PropTypes.func.isRequired
  }
  _gotoPrevPage = () =>{
    this.props.setCurrPageNum(this.props.currPage-1);
  }

  _gotoNextPage = () => {
    this.props.setCurrPageNum(this.props.currPage+1);
  }

  render() {
    return (
        <div>
          <FizzBuzzList items={this.props.list} currPage={this.props.currPage}/>
          <button className='primary-btn' onClick={this._gotoPrevPage} disabled={this.props.currPage===1}>Prev </button>
          <button className='primary-btn' onClick={this._gotoNextPage} disabled={this.props.currPage >= Math.ceil(this.props.list.length/fizzBuzzConst.MAX_SIZE_OF_PAGE) || this.props.currPage===0}> Next </button>
        </div>
    )
  }
}

export default Pagination;