import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import FizzBuzzList from './FizzBuzzList';
import * as fizzBuzzConst from '../constants/FizzBuzzConst';

const setup = ( editing = false ) => {
	const props = {
		items: [{
			key: 'FIZZ',
			val:'FIZZ'
		},{
			key: 'BUZZ',
			val:'BUZZ'
		},
		{
			key: 'FIZZBUZZ',
			val:'FIZZBUZZ'
		}],
		currPage: 1
	};

	const renderer = createRenderer();

	renderer.render(
		<FizzBuzzList {...props} />
	);

	let output = renderer.getRenderOutput();

 
	return {
		props: props,
		output: output,
		renderer: renderer
	};
};

describe('List item', () => {
	it('initial render', () => {
		const { output } = setup();

		expect(output.type).toBe('div');
		expect(output.props.children.type).toBe('ul');
	});

	it('should render three list items', () => {
		debugger;
		const { output } = setup();
		expect(output.props.children.props.children.length).toEqual(3);
		expect(output.props.children.props.children[0].type).toBe('li');
		expect(output.props.children.props.children[1].type).toBe('li');
		expect(output.props.children.props.children[2].type).toBe('li');
	});

	it('list item key should append index', () => {
		const { output } = setup();
		expect(output.props.children.props.children.length).toEqual(3);
		expect(output.props.children.props.children[0].key).toBe('FIZZ0');
		expect(output.props.children.props.children[1].key).toBe('BUZZ1');
	});

	it('list item value should be rendered', () => {
		const { output } = setup();
		expect(output.props.children.props.children[0].props.children[0]).toBe('FIZZ');
		expect(output.props.children.props.children[1].props.children[0]).toBe('BUZZ');
	});

	it('list item value should be wizz wuzz if the day is wednesday', () => {
		//setting the current date as wednesday. so that it will display as WIZZ and WUZZ
		fizzBuzzConst.WEDNESDAY_NUM = new Date().getDay();
		const { output, renderer } = setup();
		expect(output.props.children.props.children[0].props.children).toBe('WIZZ');
		expect(output.props.children.props.children[1].props.children).toBe('WUZZ');
		//both wizz and wuzz should be rendered in spans 
		expect(output.props.children.props.children[2].props.children.length).toEqual(2);
	});
});

