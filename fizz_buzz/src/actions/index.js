import * as types from '../constants/FizzBuzzConst';

export const setFizzBuzzList = fizzBuzzList => ({
	type: types.SET_FIZZ_BUZZ_LIST,
	fizzBuzzList
});

export const setCurrPageNum = currPageNo => ({
	type: types.SET_CURR_PAGE_NUM,
    currPageNo: currPageNo
});