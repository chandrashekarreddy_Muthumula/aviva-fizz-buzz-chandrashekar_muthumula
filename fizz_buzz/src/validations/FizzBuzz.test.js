import FizzBuzz from './FizzBuzz';

describe('FizzBuzz validation', () => {
	it('Should validate fizzbuzz', () => {
		//debugger;
		expect(FizzBuzz(1)).toBeTruthy();
		expect(FizzBuzz(2000)).toBe(false);
		expect(FizzBuzz('asdf')).toBe(false);
	});
});