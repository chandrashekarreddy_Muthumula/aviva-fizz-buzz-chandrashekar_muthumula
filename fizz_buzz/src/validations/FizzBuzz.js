import * as fizzBuzzConst from '../constants/FizzBuzzConst';
const validateFizzBuzz = (maxFizzBuzzNo) => {
	return !isNaN(maxFizzBuzzNo) && maxFizzBuzzNo <=  fizzBuzzConst.MAX_FIZZ_BUZZ_INPUT && maxFizzBuzzNo >= fizzBuzzConst.MIN_FIZZ_BUZZ_INPUT;
};
export default validateFizzBuzz;
