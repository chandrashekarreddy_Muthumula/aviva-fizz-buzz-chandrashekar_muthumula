import { combineReducers } from 'redux';
import fizzBuzz from './fizzBuzz';
import Pagination from './Pagination';

const rootReducer = combineReducers({
	fizzBuzz,
	Pagination
});

export default rootReducer;
