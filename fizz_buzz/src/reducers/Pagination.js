const Pagination = (state = 0, action) => {
	switch(action.type){
	case 'SET_FIZZ_BUZZ_LIST':
		return {currPage: 1};
	case 'SET_CURR_PAGE_NUM':
		return {currPage: action.currPageNo};
	case 'NEXT':
		return state.currPage++;
	case 'PREV':
		return state.currPage--;
	default:
		return state;
	}
};

export default Pagination;
