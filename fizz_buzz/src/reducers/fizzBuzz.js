const fizzBuzz = (state = 0, action) => {
	switch (action.type) {
	case 'SET_FIZZ_BUZZ_LIST':
		return {fizzBuzzList: action.fizzBuzzList};
	default:
		return state;

	}
};
export default fizzBuzz;
