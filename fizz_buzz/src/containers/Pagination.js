import { connect } from 'react-redux';
import { setCurrPageNum } from '../actions';
import Pagination from '../components/Pagination';
const mapStateToProps = state => ({
	currPage: state.Pagination.currPage || 1
});
export default connect(mapStateToProps, {setCurrPageNum})(Pagination);
