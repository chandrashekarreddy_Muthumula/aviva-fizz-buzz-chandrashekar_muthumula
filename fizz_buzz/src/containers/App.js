import { connect } from 'react-redux';
import App from '../components/App';
import { setFizzBuzzList } from '../actions';

const mapStateToProps = state => ({
	fizzBuzzList: state.fizzBuzz.fizzBuzzList || [],
	currPage: state.fizzBuzz.currPage || 1
});

export default connect(mapStateToProps, {setFizzBuzzList})(App);
