export const NEXT='NEXT';
export const PREV = 'PREV';
export const MIN_FIZZ_BUZZ_INPUT = 1;
export const MAX_FIZZ_BUZZ_INPUT = 1000;
export const SET_FIZZ_BUZZ_LIST = 'SET_FIZZ_BUZZ_LIST';
export const SET_CURR_PAGE_NUM = 'SET_CURR_PAGE_NUM';
export const MAX_SIZE_OF_PAGE = 20;
export const FIZZ_BUZZ_KEYS = {
	'FIZZ': 'FIZZ',
	'BUZZ': 'BUZZ',
	'FIZZBUZZ':'FIZZBUZZ'
};
export const WEDNESDAY_NUM = 3;
export const WEDNESDAY_CODE = {
	'FIZZ': 'WIZZ', 'BUZZ': 'WUZZ', 'FIZZBUZZ':'WIZZWUZZ'
};
export const ERRORS = {
	'FIZZBUZZ_INVALID_INPUT': 'Please enter the fizz buzz max number between ' + MIN_FIZZ_BUZZ_INPUT + ' and ' + MAX_FIZZ_BUZZ_INPUT
};
